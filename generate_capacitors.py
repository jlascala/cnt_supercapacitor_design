import math
import gdspy

# Geometric parameters in um
#fingerLength = bestFingerLength*1e6

fingerLength = 100
fingerWidth = 32

lineWidth = 100
gap = 4

maxX = 5000#maxX*1e6
maxY = 5000#maxY*1e6

lineLength = maxY-(2*lineWidth+gap)#lineLength*1e6

siLayer = {"layer": 11, "datatype": 0}
metalLayer = {"layer": 13, "datatype": 0}
subSiLayer = {"layer": 21, "datatype": 0}
#oxideEtchLayer = {"layer": 12, "datatype": 0}
catalystLayer = {"layer": 12, "datatype": 0}

dicingLayer = {"layer": 53, "datatype": 0}

def create_dicing_mark(lib):
    cell = lib.new_cell("dicing_mark")
    # Create a polygon from a list of vertices
    points = [(0, 0), (525, 0), (525, 525), (500, 525), (500, 25), (0, 25)]
    mark1 = gdspy.Polygon(points, **dicingLayer).translate(-550,25)
    mark2 = gdspy.copy(mark1).mirror((0,525),(0,-525))
    mark3 = gdspy.copy(mark1).mirror((525,0),(-525,0))
    mark4 = gdspy.copy(mark3).mirror((0,525),(0,-525))
    
    cell.add(mark1)
    cell.add(mark2)
    cell.add(mark3)
    cell.add(mark4)
    
    points = [(0, 0), (37.5, 0), (37.5, -37.5), (62.5, -37.5), (62.5, 0), (100, 0), (100, 25), (62.5, 25), (62.5, 62.5), (37.5, 62.5), (37.5, 25), (0, 25)]
    cross = gdspy.Polygon(points, **dicingLayer).translate(-50,-12.5)
    cell.add(cross)
    
    return cell

def create_dicing_grid(lib, xLength,yLength, xOffset=0.0,yOffset=0.0, waferDiameter=100000):
    mark = create_dicing_mark(lib)
    nbChipsX=math.ceil(waferDiameter/xLength)+1
    nbChipsY=math.ceil(waferDiameter/yLength)+1
    print(f'Dicing grid : ({nbChipsX},{nbChipsY}) chips.')
    return gdspy.CellArray(mark, nbChipsX, nbChipsY, (xLength, yLength), (xOffset,yOffset))
    
    #for i in range(nbChipsX):
    #    for j in range(nbChipsY):
            

def create_finger(cell, position=(0,0),fingerLength=fingerLength,fingerWidth=fingerWidth, horizontal=True):
    if horizontal:
        w = fingerWidth#*1e6
        l = fingerLength#*1e6
    else:
        l = fingerWidth#*1e6
        w = fingerLength#*1e6
    
    metalF = gdspy.Rectangle((0,0),(l,w),**metalLayer).translate(position[0],position[1])
    #oxideEtchF = gdspy.Rectangle((1,1),(l-1,w-1),**oxideEtchLayer).translate(position[0],position[1])
    catalystF = gdspy.Rectangle((0,0),(l,w),**catalystLayer).translate(position[0],position[1])
    cell.add(metalF)
    #cell.add(oxideEtchF)
    cell.add(catalystF)
    return cell
    
def create_line(cell, position = (0,0), lineLength=lineLength,lineWidth=lineWidth, horizontal=False):
    w = lineWidth#*1e6
    l = lineLength#*1e6
    si = None
    me = None
    if horizontal:
        si = gdspy.Rectangle((0,0),(l,w), **subSiLayer).translate(position[0],position[1])
        me = gdspy.Rectangle((0,0),(l,w), **metalLayer).translate(position[0],position[1])
    else:
        si = gdspy.Rectangle((0,0),(w,l), **subSiLayer).translate(position[0],position[1])
        me = gdspy.Rectangle((0,0),(w,l), **metalLayer).translate(position[0],position[1])
    cell.add(si)
    cell.add(me)
    return cell
    
def create_capacitor_structure(lib,\
                               fingerLength = 300, fingerWidth = 2,\
                               lineWidth = 100, lineLength = None,\
                               gap = 2, maxX = 5000, maxY = 5000, name = None):
    catalystPrecursor = 4
    cell = None
    if name is not None:
        cell = lib.new_cell('structure_'+name)
    else:
        cell = lib.new_cell('structure_')

    if lineLength is None:
        lineLength = maxY-(2*lineWidth+gap)

    # Create the geometry (a single rectangle) and add it to the cell.
    #rect = gdspy.Rectangle((0, 0), (2, 1))
    #cell.add(rect)

    nbCap = math.floor((maxX-lineWidth)/(lineWidth + fingerLength + gap))
    print("The finger width is " + str(fingerWidth))
    nbFingers = math.floor((maxY-(2*(lineWidth+gap)))/(fingerWidth+gap))

    print(f'Number of caps in parallel: {nbCap}, number of fingers per cap: {nbFingers}')

    cell.add(gdspy.Rectangle((0,0),(maxX,maxY), **siLayer))

    create_line(cell,position=(0,maxY-lineWidth),lineLength=maxX,lineWidth =lineWidth, horizontal=True)
    create_line(cell,position=(lineWidth+gap+fingerLength,0),lineLength=maxX-lineWidth-gap-fingerLength, lineWidth = lineWidth, horizontal=True)
    #cell.add(create_line(lineLength=maxX-lineWidth-2*gap-fingerLength, horizontal=True).translate(lineWidth+gap,0))

    create_line(cell,lineLength=maxY,lineWidth=lineWidth)
    x = 0
    y = lineWidth
    for j in range(math.ceil(nbFingers/2)):
            create_finger(cell,position=(x-(catalystPrecursor/2),j*(2*fingerWidth+2*gap)+y), fingerWidth=fingerWidth,fingerLength = catalystPrecursor)

    for i in range(nbCap):
        if i%2 == 0:
            x = (i+1)*(lineWidth+gap+fingerLength)
            y = lineWidth+gap+fingerWidth
            create_line(cell,position=(x,y),lineLength=lineLength-fingerWidth, lineWidth = lineWidth) # UP
            for j in range(math.ceil(nbFingers/2)-1):
                #create_finger(cell,position=(x-fingerLength,j*(2*fingerWidth+2*gap)+y), fingerWidth=fingerWidth,fingerLength = catalystPrecursor)
                create_finger(cell,position=(x-(catalystPrecursor/2),j*(2*fingerWidth+2*gap)+y), fingerWidth=fingerWidth,fingerLength = catalystPrecursor)
                if i<nbCap-1 or nbCap%2 == 0:
                    #create_finger(cell,position=(x+lineWidth,j*(2*fingerWidth+2*gap)+y), fingerWidth=fingerWidth,fingerLength = catalystPrecursor)
                    create_finger(cell,position=(x+lineWidth-(catalystPrecursor/2),j*(2*fingerWidth+2*gap)+y), fingerWidth=fingerWidth,fingerLength = catalystPrecursor)
        else:
            x = (i+1)*(lineWidth+gap+fingerLength)
            y = lineWidth#+gap
            create_line(cell,position=(x,y),lineLength = lineLength, lineWidth = lineWidth) # UP
            for j in range(math.ceil(nbFingers/2)):
                #create_finger(cell,position=(x-fingerLength,j*(2*fingerWidth+2*gap)+y), fingerWidth=fingerWidth,fingerLength = catalystPrecursor)
                create_finger(cell,position=(x-(catalystPrecursor/2),j*(2*fingerWidth+2*gap)+y), fingerWidth=fingerWidth,fingerLength = catalystPrecursor)
                if i<nbCap-1 or nbCap%2 == 1:
                    #create_finger(cell,position=(x+lineWidth,j*(2*fingerWidth+2*gap)+y), fingerWidth=fingerWidth,fingerLength = catalystPrecursor)
                    create_finger(cell,position=(x+lineWidth-(catalystPrecursor/2),j*(2*fingerWidth+2*gap)+y), fingerWidth=fingerWidth,fingerLength = catalystPrecursor)
                    
    return cell

def create_capacitor_chip(lib,
                          fingerLength = 300, fingerWidth = 10,\
                          lineWidth = 100, lineLength = None,\
                          gap = 10, maxX = 5000, maxY = 5000,\
                         name = None, additionalText=None):
    chipSizeX = 8000
    chipSizeY = 10000
    
    if name is None:
        name = f'Fl{math.floor(fingerLength)}/Fw{math.floor(fingerWidth)}/Lw{math.floor(lineWidth)}/gap{math.floor(gap)}'
    capChip = lib.new_cell("capacitor_"+name)
    capStruct = create_capacitor_structure(lib, fingerLength = fingerLength, fingerWidth = fingerWidth,\
                               lineWidth = lineWidth, lineLength = None,\
                               gap = gap, maxX = maxX, maxY = maxY, name = name)
    
    descriptionTextSize = 180
    descriptionX = 1000
    descriptionY = 9500
    capChip.add(gdspy.Text(f'finger length = {math.floor(fingerLength)} um\n\
finger width = {math.floor(fingerWidth)} um\n\
line width = {lineWidth} um\n\
gap = {gap} um',descriptionTextSize, (descriptionX,descriptionY),**siLayer))
    
    points = [(2300,60), (4900,60), (4900,2400),(2500+lineWidth,2400),(2500+lineWidth,7500), (2300,7500)]
    capChip.add(gdspy.Polygon(points,**metalLayer).translate(-1000,0))
    
    points = [(5100,60), (7500,60), (7500,2500+lineWidth), (5100,2500+lineWidth)]
    capChip.add(gdspy.Polygon(points,**metalLayer).translate(-1000,0))
    
    capChip.add(gdspy.CellReference(capStruct, (1500,2500)))
    
    points = [(0, 0), (525, 0), (525, 525), (500, 525), (500, 25), (0, 25)]
    pointsCross = [(0, 0), (50, 0), (50, 50), (37.5, 50), (37.5, 12.5), (0, 12.5)]
    side1 = gdspy.Polygon(points, **dicingLayer).translate(chipSizeX-550,25)
    cross1 = gdspy.Polygon(pointsCross, **dicingLayer).translate(chipSizeX-50,0)
    side2 = gdspy.copy(side1).mirror((0,chipSizeY/2),(chipSizeX,chipSizeY/2))
    side3 = gdspy.copy(side1).mirror((chipSizeX/2,0),(chipSizeX/2,chipSizeY))
    side4 = gdspy.copy(side3).mirror((0,chipSizeY/2),(chipSizeX,chipSizeY/2))
    
    cross2 = gdspy.copy(cross1).mirror((0,chipSizeY/2),(chipSizeX,chipSizeY/2))
    cross3 = gdspy.copy(cross1).mirror((chipSizeX/2,0),(chipSizeX/2,chipSizeY))
    cross4 = gdspy.copy(cross3).mirror((0,chipSizeY/2),(chipSizeX,chipSizeY/2))
    
    capChip.add(side1)
    capChip.add(side2)
    capChip.add(side3)
    capChip.add(side4)
    capChip.add(cross1)
    capChip.add(cross2)
    capChip.add(cross3)
    capChip.add(cross4)
    
    return capChip

lib = gdspy.GdsLibrary(infile="template_cmi_100mm.gds")

# Geometry must be placed in cells.
top = lib.cells["TOP_CELL"]


contactResistivityCNTs = 45.4e-6
resistivityElectrolyte = 1

lineWidths = [4,8,8,16]
gap = 4
fingerLengths = [40,80,80,160,160,320,320,640,640,1280]
i = 0
j = 0
capacitors = lib.new_cell("capacitors")
for lineWidth in lineWidths:
    for fingerLength in fingerLengths:
        nbCap = math.floor((maxX-lineWidth)/(lineWidth + fingerLength + gap))
        #fingerLength = math.floor((maxX-lineWidth)/nbCap)
        fingerLength = math.floor((maxX-(nbCap+1)*(lineWidth+gap))/nbCap)
        fingerWidth = math.floor(math.sqrt((fingerLength*1e-6*contactResistivityCNTs)/resistivityElectrolyte)*1e6)

        nbFingers = math.floor((maxY-(2*(lineWidth+gap)))/(fingerWidth+gap))

        fingerWidth = math.floor((maxY-(2*(lineWidth+gap)))/nbFingers)-gap

        capacitors.add(gdspy.CellReference(create_capacitor_chip(lib, fingerLength=fingerLength, gap = gap,
                                            fingerWidth=fingerWidth,lineWidth=lineWidth,name=f"{i}{j}"),(8000*i,10000*j)))
        i = i + 1
    j = j+1
    i=0
    
top.add(gdspy.CellReference(capacitors, (-50000,-50000)))

# Dicing
#top.add(create_dicing_grid(lib,10120,10120,-50600,-50600))

# Save the library in a file.
lib.write_gds('capacitor_struct.gds')

# Optionally, save an image of the cell as SVG.
#cell.write_svg('2020_02_21_capacitor_struct.svg')

# Display all cells using the internal viewer.
#gdspy.LayoutViewer()
