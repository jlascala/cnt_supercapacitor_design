# CNT supercapacitor design

This repository contains the code for generating supercapacitors mask in the file [generate_capacitors.py](./generate_capacitors.py).

The notebook as well as the underlaying functions for computing the optimized geometry are to be found in the folder [geometry_optimization](./geometry_optimization). Note that two notebooks are present and only the second one, [geometry_optimization2.ipynb](./geometry_optimization/geometry_optimization2.ipynb), contains the last developments.

Finally, the designed mask is available in the folder _mask_, under the name [process_2B_test1.gds](./mask/process_2B_test1/process_2B_test1.gds)

