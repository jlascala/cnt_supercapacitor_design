import numpy as np
import math
import matplotlib.pyplot as plt

# Default geometric parameters
maxX = 10e-3
maxY = 10e-3

metalThickness = 100e-9
fingerLength = 10e-3
fingerWidth = 20e-6

gap = fingerWidth

lineWidth = 10e-6
lineLength = maxY-2*lineWidth-gap
resistivityCNTs = 1.75e-5
cNTsThickness = 1e-6
contactResistivityCNTs = 45.4e-6
resistivityElectrolyte = 1/1.15 # From https://www.sigmaaldrich.com/catalog/product/sial/00768?lang=fr&region=FR


# CNT parameters

CNTParameters = {\
    "diameter":8e-9,\
    "density":1e12,\
    "height":fingerLength}

electrolyteParameters = {\
    "double_layer_thickness":5e-10,\
    "relative_permittivity":11.7}


def compute_C(A, CNTParameters=CNTParameters,electrolyteParameters=electrolyteParameters):
    f=0.571
    e0 = 8.854e-12
    S_CNT = math.pi*CNTParameters["diameter"]*CNTParameters["height"]
    Ael = A*S_CNT*CNTParameters["density"]*f
    C = (Ael*e0*electrolyteParameters["relative_permittivity"])/(2*electrolyteParameters["double_layer_thickness"])
    return C
    
    

def compute_C_and_R(fingerLength=fingerLength,\
              fingerWidth=fingerWidth,\
             lineWidth=lineWidth,\
             lineLength=lineLength,\
             gap=gap,\
             maxX=maxX,\
             maxY=maxY,\
             metalThickness=metalThickness,\
			cNTsThickness = cNTsThickness,\
			resistivityElectrolyte = resistivityElectrolyte):
    nbCap = math.floor((maxX-lineWidth)/(lineWidth + fingerLength + gap))
    if nbCap <1:
        nbCap = 1
    #print(nbCap)
    resistivity=1/(9.66e6)
    nbFingers = math.floor((maxY-(2*(lineWidth+gap)))/(fingerWidth+gap))
    if nbFingers <1:
        nbFingers = 1
    epsilon = 1
    dielectricThickness = 1e-10
    Afingers = nbCap*nbFingers*(fingerLength * fingerWidth)
    AgrowthCNTs = nbCap*nbFingers*fingerWidth*cNTsThickness
    Ametal = (nbCap+3)*(lineWidth*lineLength)

    #CNTParameters["height"] = cNTsThickness
    CNTParameters["height"] = fingerLength

    #C = Afingers*epsilon/(dielectricThickness)
    C = compute_C(AgrowthCNTs,CNTParameters)
    #C = compute_C(Afingers,CNTParameters)

    #R = resistivity*((lineLength/(lineWidth*metalThickness))+((fingerLength)/(metalThickness*fingerWidth)))/nbCap
    #R = resistivity*((lineLength/(lineWidth*metalThickness)))/nbCap
    Rlines = resistivity*((lineLength/(lineWidth*metalThickness)))/nbCap
    RfingersCNTs = (resistivityCNTs*((fingerLength/(2*fingerWidth*cNTsThickness))))/nbFingers/nbCap
    RfingersContact = (contactResistivityCNTs/(fingerWidth*cNTsThickness))/nbFingers/nbCap
    Relectrolyte = (resistivityElectrolyte*(gap+fingerWidth)/(cNTsThickness*fingerLength))/nbFingers/nbCap
    #R = resistivity*((lineLength/(lineWidth*metalThickness)))/nbCap+\
    #    (resistivityCNTs*((fingerLength/(2*fingerWidth*cNTsThickness)))+\
    #    (contactResistivityCNTs/(fingerWidth*cNTsThickness)))/nbFingers/nbCap+\
    #    (resistivityElectrolyte*(gap+fingerWidth)/(cNTsThickness*fingerLength))/nbFingers/nbCap
    
    return C,Rlines,RfingersCNTs,RfingersContact,Relectrolyte,Afingers,Ametal

def get_best_finger_length(fingerLength=fingerLength,\
             fingerWidth=fingerWidth,\
             lineWidth=lineWidth,\
             lineLength=lineLength,\
             gap=gap,\
             maxX=maxX,\
             maxY=maxY,\
             metalThickness=metalThickness,\
             cNTsThickness = cNTsThickness,\
             resistivityElectrolyte = resistivityElectrolyte):
    fingerLengthList = np.logspace(-6, -2, 20000)
    ratioCR =  []
    for x in fingerLengthList:
        #print(C,R)
        C,Rlines,RfingersCNTs,RfingersContact,Relectrolyte,A,Ametal = compute_C_and_R(x,\
             fingerWidth=fingerWidth,\
             lineWidth=lineWidth,\
             lineLength=lineLength,\
             gap=gap,\
             maxX=maxX,\
             maxY=maxY,\
             metalThickness=metalThickness,\
             cNTsThickness = cNTsThickness,\
             resistivityElectrolyte = resistivityElectrolyte)
        ratioCR.append(C/(Rlines+RfingersCNTs+RfingersContact+Relectrolyte))
        
    import operator
    index, value = max(enumerate(ratioCR), key=operator.itemgetter(1))
        
    bestFingerLength = fingerLengthList[index]
        
    return bestFingerLength
